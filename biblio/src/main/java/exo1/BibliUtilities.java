package exo1;

import java.util.ArrayList;

public class BibliUtilities {
	
	
	
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref) {
		ArrayList<NoticeBibliographique> res =new ArrayList<NoticeBibliographique>();
		ArrayList<NoticeBibliographique> allnotice= new GlobalBibliographyAccess().noticesDuMemeAuteurQue(ref);
		int i=0;
		int b=0;
		while (i<5 && b<allnotice.size()){
			if(allnotice.get(i).getTitre()!=ref.getTitre()) {
				if(!res.contains(allnotice.get(i))) {
					res.add(allnotice.get(i)); 
					i++;
			
				}
			}
			b++;
		}
		return res;
			
		
		
	}

}
